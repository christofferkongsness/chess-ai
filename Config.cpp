#include "Config.h"

float weightChangeMutation = 10.0f;
float newNodeMutation = 20.0f;
float newColumnMutation = 40.0f;
float newLinkMutation = 1.0f;

float deleteNodeMutation = 20.0f;
float deleteLinkMutation = 0.5f;

int ageModifierLimit = 100;
int maxColumnsInAGene = 1000000;
int maxNodesInGene = 1000000;
int maxLinksOutOfNode = 4;

float breedingThreshold = 0.4f;
float energyCostForBreeding = 0.2f;

int lengthBetweenNodes = 40;
int lengthBetweenGenes = 600;

std::random_device randomSeeder;
std::mt19937 randomGen = std::mt19937(randomSeeder());
std::uniform_real_distribution<float> percentageDistributor = std::uniform_real_distribution<float>(0.0f, 100.0f);
std::uniform_int_distribution<int> coinflipDistributor = std::uniform_int_distribution<int>(0, 1);