#include "Actor.h"

Actor::Actor(Models *model, glm::vec3 position, ChessPieces type, Color color, glm::vec3 scale, int id)
{
	this->model = model;
	this->position = position;
	this->type = type;
	this->color = color;
	this->id = id;
	this->scale = scale;
	modelMatrix = glm::translate(glm::mat4(1.0f), position) * glm::scale(glm::mat4(1.0f), scale);
	DrawBool = true;
}

void Actor::draw(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler)
{
	if (DrawBool)
	{
		model->draw(camera, shaderProgram, lightHandler, &modelMatrix, color, id);
	}
}

Actor::~Actor()
{
	
}

void Actor::update()
{
	modelMatrix = glm::translate(glm::mat4(1.0f), position) * glm::scale(glm::mat4(1.0f), scale);
}
