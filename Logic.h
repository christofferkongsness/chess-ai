#pragma once
#include "SceneGraph.h"
#include "Genome.h"

const int CHESSROW = 8;
const int MAXTURN = 400;

class SceneGraph;

struct moveInfo
{
	int startX, startY;
	int endX, endY;
	ChessPieces typeMoved;
	ChessPieces atLocation;

	float score;
};

//struct tile
//{
//	//follow the tile
//	int x, y; // X here is the first row of the vector as int [x][y] so x is numbers while y is letters
//	int tileId;
//	// Follows the piece
//	ChessPieces type;
//	int side;		// side is ether -1 = empty, 0 = white, 1 = black
//	int pieceId;	
//	Move move;
//	bool castlingRights;
//	int pawnCounter;
//};

class Logic
{
public:
	Logic();
	~Logic();
	bool checkForLegalMoves(int id);
	std::vector<std::vector<tile>>* getChessBoard();
	std::vector<std::vector<Move>> LegalMoves();
	std::vector<std::vector<Move>> AllLegalMoves();
	void doLegalMove(int pieceID, glm::vec2 tileId, ChessPieces pawnUpgradeType = ChessPieces::NOPIECE);
	glm::vec2 upgradePawn(int actor);
	void reset();
	bool checkTurn(int side);
	bool check(int side);
	bool checkMate(int side);
	std::vector<moveInfo> getAllMoves();
	void resetLogic();
	void aiMove();

	bool getPawnUpgrade() { return pawnUpgrade; }
	std::string getStockfishString() { return stockfishBoardString; }
	std::string getFEN();
	void setPawnUpgrade(bool pawnUpgrade) { this->pawnUpgrade = pawnUpgrade; }
	int getGameCount() {return gameCount; }
	void setGameCount(int gameCount) {this->gameCount = gameCount; }
	void getScreenInfo(int &piece, int &newLoc);

	//TRAINING FUNCTIONS #Duct Tape
	void aiTrain();
	void doLegalMoveTrain(int pieceID, glm::vec2 tileNew, ChessPieces pawnUpgradeType);
	bool checkMateTrain(int side);
private:
	void checkPawn(glm::vec2 piece, tile pieceLoacation);
	void checkKnight(glm::vec2 piece, tile pieceLoacation);
	void checkBishop(glm::vec2 piece, tile pieceLoacation);
	void checkRook(glm::vec2 piece, tile pieceLoacation);
	void checkQueen(glm::vec2 piece, tile pieceLoacation);
	void checkKing(glm::vec2 piece, tile pieceLoacation);
	tile findKingTile(int side);
	bool illegalMove(glm::vec2 piece, glm::vec2 newPosition);
	void addToStockfishBoardString(glm::vec2 start, glm::vec2 finish);
	void addMove(glm::vec2 start, glm::vec2 finish, ChessPieces pawnUpgrade = ChessPieces::NOPIECE);
	bool treeSteps();


	std::string stockfishBoardString;
	std::vector<moveInfo> moves;
	std::vector<std::vector<tile>> chessBoard;
	std::vector<std::vector<tile>> boardAfterMove; // made to copy the boardstate;
	glm::vec2 findPiece(int id);
	glm::vec2 findTile(int id);
	glm::vec2 pawnToBeUpgraded;
	int turn;
	int genomeIDCounter;
	int turnCounter;
	bool pawnUpgrade = false;
	int pieceMoved;

	int fiftyMovesCounter = 0;
	glm::vec2 newLocation;

	Genome* whiteBrain; 
	Genome* blackBrain;
	std::string enPassanteTile;

	int gameCount;

};
