#include "SceneGraph.h"

void SceneGraph::update()
{
	for (size_t i = 0; i < actors.size(); i++)
	{
		actors.at(i).update();
	}
}

void SceneGraph::update1(std::vector<std::vector<Move>>* moves)
{
	int f = 0;
	for (size_t i = 0; i < moves->size(); i++)
	{
		for (size_t g = 0; g < moves->at(i).size(); g++)
		{
			moveBlock.at(f).update(moves->at(i).at(g));
			f++;
		}
	}
}

void SceneGraph::updateNew()
{
	actors.clear();
	Color tempColor;
	Actor temp = Actor(modelHandler->getModel("Board"), glm::vec3(0.0f, -1.0f, 0.0f), ChessPieces::BOARD, Color::BOARDCOLOR, scale, actors.size());
	actors.push_back(temp);
	for (size_t i = 0; i < chessBoard->size(); i++)
	{
		for (size_t f = 0; f < chessBoard->at(i).size(); f++)
		{
			if (chessBoard->at(i).at(f).side == 0)
			{
				tempColor = WHITE;
			}
			else if (chessBoard->at(i).at(f).side == 1)
			{
				tempColor = BLACK;
			}
			else
			{
				tempColor = NEUTRAL;
			}

			switch (chessBoard->at(i).at(f).type)
			{
			case NOPIECE: break;
			case PAWN:
			{Actor temp(modelHandler->getModel("Pawn"), glm::vec3(i * tileLength, 0, f * tileHight) + boardOffset, chessBoard->at(i).at(f).type, tempColor, scale, actors.size());
			actors.push_back(temp); break; }

			case KNIGHT: {Actor temp(modelHandler->getModel("Knight"), glm::vec3(i * tileLength, 0, f * tileHight) + boardOffset, chessBoard->at(i).at(f).type, tempColor, scale, actors.size());
				actors.push_back(temp); break; }
			case BISHOP: {Actor temp(modelHandler->getModel("Bishop"), glm::vec3(i * tileLength, 0, f * tileHight) + boardOffset, chessBoard->at(i).at(f).type, tempColor, scale, actors.size());
				actors.push_back(temp); break; }
			case ROOK: {Actor temp(modelHandler->getModel("Rook"), glm::vec3(i * tileLength, 0, f * tileHight) + boardOffset, chessBoard->at(i).at(f).type, tempColor, scale, actors.size());
				actors.push_back(temp); break; }
			case QUEEN: { Actor temp(modelHandler->getModel("Queen"), glm::vec3(i * tileLength, 0, f * tileHight) + boardOffset, chessBoard->at(i).at(f).type, tempColor, scale, actors.size());
				actors.push_back(temp); break; }
			case KING: {Actor temp(modelHandler->getModel("King"), glm::vec3(i * tileLength, 0, f * tileHight) + boardOffset, chessBoard->at(i).at(f).type, tempColor, scale, actors.size());
				actors.push_back(temp); break; }
			}
		}
	}
}

glm::vec2 SceneGraph::update2(int piece, int Location)
{
	glm::vec2 pos = glm::vec2(500, 500);
	std::cout << "LOCATION " << Location - 33 << "    actors: " << actors.size() << std::endl;
	if (actors.at(piece).getType() == KING)
	{
		castling(piece, Location);
	}
	if (Location < 32)
	{
		glm::vec3 temp = actors.at(Location).getPos();
		glm::vec2 xy;
		xy.x = (temp.x - boardOffset.x) / tileLength;
		xy.y = ((temp.z - boardOffset.z) / tileHight) + 0.1;
		xy = closestWhole(xy);
		//std::cout << x << "x" << y << "y \n";
		if (moveBlock.at((8 * xy.x) + xy.y).getColor() != Color::NONELEAGALSPACE && moveBlock.at((8 * xy.x) + xy.y).getColor() != Color::OWNPIECE)
		{
			actors.at(piece).setPos(actors.at(Location).getPos());
			actors.at(Location).setDrawBool(false);
			pos = xy;
			//std::cout << pos.x << " x" << pos.y << "y\n";
			//std::cout << pos.x * 8 + pos.y << " =? "<< Location <<" Location\n";
		}
		else
		{
			pos = glm::vec2(500, 500);
		}
	}
	else if (Location > 32)
	{
		if (moveBlock.at(Location - actors.size()).getColor() != Color::OWNPIECE)
		{
			int temp = findLocation(moveBlock.at(Location - actors.size()));
			if (findLocation(moveBlock.at(Location - actors.size())) != -1)
			{
				actors.at(temp).setDrawBool(false);
			}
			actors.at(piece).setPos(glm::vec3(moveBlock.at(Location - actors.size()).getXandY().x * tileLength, 0, moveBlock.at(Location - actors.size()).getXandY().y * tileHight) + boardOffset);
			pos = moveBlock.at(Location - actors.size()).getXandY();

			std::cout << pos.x << " x" << pos.y << "y\n";
			std::cout << pos.x * 8 + pos.y << " =? " << Location - actors.size() << " Location\n";
		}
	}
	for (size_t i = 0; i < moveBlock.size(); i++)
	{
		moveBlock.at(i).reset();
	}
	pos = glm::vec2(pos.y, pos.x);
	return closestWhole(pos);
}

void SceneGraph::draw(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler)
{
	for (size_t i = 0; i < actors.size(); i++)
	{
		actors.at(i).draw(camera, shaderProgram, lightHandler);
	}
	for (size_t i = 0; i < moveBlock.size(); i++)
	{
		moveBlock.at(i).draw(camera, shaderProgram, lightHandler);
	}
	if (drawChange)
	{
		for (size_t i = 0; i < pawnChangeActors.size(); i++)
		{
			pawnChangeActors.at(i).draw(camera, shaderProgram, lightHandler);
		}
	}
}

void SceneGraph::draw2(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler)
{
	for (size_t i = 0; i < actors.size(); i++)
	{
		actors.at(i).draw(camera, shaderProgram, lightHandler);
	}
}

SceneGraph::SceneGraph(ModelHandler* modelHandler, std::vector<std::vector<tile>>* chessBoard)
{
	this->chessBoard = chessBoard;
	this->modelHandler = modelHandler;
	glm::vec3 size = modelHandler->getModel("Board")->size();
	glm::vec3 smallSize = modelHandler->getModel("Board")->sizeDiffrence();
	boardOffset = (size - smallSize) / glm::vec3(2);
	size *= scale;
	smallSize *= scale;
	boardOffset *= scale;
	tileHight = smallSize.z / 8;
	tileLength = smallSize.x / 8;
	glm::vec3 offset = glm::vec3(tileLength / 2.0f, 0, tileHight / 2.0f);
	boardOffset += offset;

}

void SceneGraph::pawnUpgrade(int type, int actor)
{
	Models* tempModel = modelHandler->getModel("Pawn");;
	std::cout << type << "type\t" << actor << "tile" << std::endl;
	switch (type)
	{
	case 100:tempModel = modelHandler->getModel("Queen"); break;
	case 101:tempModel = modelHandler->getModel("Knight"); break;
	case 102:tempModel = modelHandler->getModel("Bishop"); break;
	case 103:tempModel = modelHandler->getModel("Rook"); break;
	}
	if (tempModel != nullptr)
	{
		actors.at(actor).setModel(tempModel);
	}

	pawnChange(false);
}

void SceneGraph::pawnChange(bool change)
{
	for (size_t i = 0; i < pawnChangeActors.size(); i++)
	{
		pawnChangeActors.at(i).setDrawBool(change);
	}
}

SceneGraph::~SceneGraph()
{
}

int SceneGraph::findLocation(MoveBlock block)
{
	glm::vec3 temp = glm::vec3(block.getXandY().x * tileLength, 0, block.getXandY().y * tileHight) + boardOffset;
	glm::vec2 temp2 = glm::vec2(temp.x, temp.y);
	temp2 = closestWhole(temp2);
	for (size_t i = 0; i < actors.size(); i++)
	{
		if (actors.at(i).getPos().x >= temp.x - 1 && actors.at(i).getPos().x <= temp.x + 1 && actors.at(i).getPos().z >= temp.z - 1 && actors.at(i).getPos().z <= temp.z + 1)
		{
			return i;
		}
	}
	return -1;
}

void SceneGraph::castling(int piece, int Location)
{
	if (Location > 32)
	{
		glm::vec3 temp = actors.at(piece).getPos();
		float tempFloatX = ((temp.x - boardOffset.x) / tileLength);
		float tempFloatY = ((temp.z - boardOffset.z) / tileHight);
		std::cout << (temp.x - boardOffset.x) / tileLength << "x\n";
		if (tempFloatY == 0 && tempFloatX == 3)  // position of king is starting pos
		{
			if (Location == 9 + 32)	//pos moved is castling spot
			{
				std::cout << "checking";
				glm::vec3 rook = actors.at(16).getPos();
				actors.at(16).setPos(glm::vec3(2 * tileLength, 0, 0) + boardOffset);
			}
			if (Location == 41 + 32)
			{
				glm::vec3 rook = actors.at(17).getPos();
				actors.at(17).setPos(glm::vec3(4 * tileLength, 0, 0) + boardOffset);
			}
		}
		else if (tempFloatY >= 6.95f && tempFloatY <= 7.05f && tempFloatX == 3)
		{
			if (Location == 16 + 32)	//pos moved is castling spot
			{
				std::cout << "checking";
				glm::vec3 rook = actors.at(18).getPos();
				actors.at(18).setPos(glm::vec3(2 * tileLength, 0, 7 * tileHight) + boardOffset);
			}
			if (Location == 48 + 32)
			{
				glm::vec3 rook = actors.at(19).getPos();
				actors.at(19).setPos(glm::vec3(4 * tileLength, 0, 7 * tileHight) + boardOffset);
			}
		}
	}
}

glm::vec2 SceneGraph::closestWhole(glm::vec2 number)
{
	glm::vec2 temp, temp2;
	temp.x = (int)number.x;
	temp.y = (int)number.y;
	temp2 = number - temp;
	if (temp2.x > 0.50)
	{
		temp.x++;
	}
	if (temp2.y > 0.50)
	{
		temp.y++;
	}
	return temp;
}