#pragma once


#if defined(__linux__)
#include <glm/glm.hpp>
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <glm\glm.hpp>
#endif
#include "ModelHandler.h"

enum ChessPieces
{
	NOPIECE,
	PAWN,
	KNIGHT,
	BISHOP,
	ROOK,
	QUEEN,
	KING,

	BOARD
};

class Actor // Hector
{
public:
	Actor(Models *model, glm::vec3 position, ChessPieces type, Color color, glm::vec3 scale, int id);
	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler);
	~Actor();

	void setPos(glm::vec3 newPos) { position = newPos; }
	void setDrawBool(bool newDrawBool) { DrawBool = newDrawBool; }
	void setModel(Models* model) { this->model = model; }
	glm::vec3 getPos() { return position; }
	ChessPieces getType() { return type; }
	void update();

private:
	glm::mat4 modelMatrix;
	glm::vec3 position, scale;

	Models* model;
	ChessPieces type;
	Color color;
	int id;
	bool DrawBool;
};
