#include "Models.h"
#include <iostream>

Models::Models()
{
}

Models::Models(std::string filePath, TextureHandler* textureHandler)
{
	this->textureHandler = textureHandler;
	loadModel(filePath);
}

void Models::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler, glm::mat4* modelMatrix, Color color, int id)
{
	for (GLuint i = 0; i < this->meshes.size(); i++)
	{
		this->meshes[i].draw(camera, shaderProgram, lightHandler, modelMatrix, color, id);
	}
}

glm::vec3 Models::size()
{
	glm::vec3 temp = glm::vec3(-100000);
	for (size_t i = 0; i < meshes.size(); i++)
	{
		if (meshes.at(i).size().x > temp.x)
		{
			temp = meshes.at(i).size();
		}
	}
	return temp;
}

glm::vec3 Models::sizeDiffrence()
{
	glm::vec3 temp = glm::vec3(-100000);
	glm::vec3 temp2 = glm::vec3(100000);
	for (size_t i = 0; i < meshes.size(); i++)
	{
		if (meshes.at(i).size().x > temp.x)
		{
			temp = meshes.at(i).size();
			 
		}
		if(meshes.at(i).size().x < temp2.x)
		{
			temp2 = meshes.at(i).size();

		}
	}
	return temp2;
	
}

void Models::loadModel(std::string path)
{
	Assimp::Importer importer;

	//aiProcess_Triangulate turns entire model into triangles (if it isn't alrady), FlipUVs flips the texture coords on y-axis where necessary
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	//checks that the scene and the root node of scene are not null, and checks one of the flags to make sure the returned data is complete.
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cerr << "Assimp Error: " << importer.GetErrorString() << std::endl;
		return;
	}
	directory = path.substr(0, path.find_last_of('/'));

	processNode(scene->mRootNode, scene);
}

void Models::processNode(aiNode * node, const aiScene * scene)
{
	//Processes all of the node's meshes (if any exist)
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene));
	}

	//Repeat the process for each of the node's children
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

Mesh Models::processMesh(aiMesh * mesh, const aiScene * scene)
{
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	GLfloat biggestSize = 0;

	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector; //temp vector in glm::vec3 format since assimp uses it's own vector class (which doesn't directly convert to glm::vec3)

		//positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		if (vector.x > biggestSize)
		{
			biggestSize = vector.x;
		}
		if (vector.y > biggestSize)
		{
			biggestSize = vector.y;
		}
		if (vector.z > biggestSize)
		{
			biggestSize = vector.z;
		}
		vertex.Position = vector;

		//Normals
	
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;
		
		//texture coordinates below (first check if the mesh contain any texcoords
		if (mesh->mTextureCoords[0])
		{
			glm::vec2 vec;
			//assumption:: We won't use models where a vertex can have multiple texture coordinates, so w always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
		{
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}
	for (int i = 0; i < vertices.size(); ++i)
	{
		//vertices.at(i).Position /= biggestSize;
	}

	//now we go through each of the mesh's faces (a face is a triangle of the mesh), and receive indices
	for (GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];

		//store all indices from the face to the indices vector
		for (GLuint j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}

	//process materials
	glm::vec3 color;
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		aiColor3D diffuseColor;
		material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);
		color.r = diffuseColor.r;
		color.g = diffuseColor.g;
		color.b = diffuseColor.b;
		

		//std::vector<Texture> diffuseMaps = this->loadMaterialTexture(material, aiTextureType_DIFFUSE, "textureDiffuse");
		//textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		//
		//std::vector<Texture> specularMaps = this->loadMaterialTexture(material, aiTextureType_SPECULAR, "textureSpecular");
		//textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	//returns a mesh object created from mesh data.
	return Mesh(vertices, indices, textures, color);
}

std::vector<Texture> Models::loadMaterialTexture(aiMaterial * mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;

	for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		bool skip = false;
		for (size_t j = 0; j < texturesLoaded.size(); j++)
		{
			if (texturesLoaded[j].path == str)
			{
				textures.push_back(texturesLoaded[j]);
				skip = true;
				break;
			}
		}
		//if the texture hasn't been loaded, do so.
		if (!skip)
		{
			//needs editing to support multiple folders (unless we want all textures in one folder)
			aiString texturePath;
			texturePath.Set("");
			texturePath.Append(str.C_Str());
			Texture tex;
			tex.id = textureHandler->createTextureFromImage(texturePath.C_Str());
			tex.type = typeName;
			tex.path = str;
			textures.push_back(tex);
			texturesLoaded.push_back(tex); //pushes back the texture to LoadedTexture (so we won't have to load the texture more than once)
		}
	}

	return textures;
}
